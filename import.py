import os
import json

import gitlab
import jira

from issue import JiraIssue, GitlabIssue

JIRA_SERVER = os.environ['JIRA_SERVER']
JIRA_USERNAME = os.environ['JIRA_USERNAME']
JIRA_PASSWORD = os.environ['JIRA_PASSWORD']
JIRA_EPIC = os.environ['JIRA_EPIC']

GITLAB_URL = os.environ['GITLAB_URL']
GITLAB_PRIVATE_TOKEN = os.environ['GITLAB_PRIVATE_TOKEN']
GITLAB_PROJECT = os.environ['GITLAB_PROJECT']

JIRA = jira.JIRA(
    options={
        'server': JIRA_SERVER,
        'mutual_authentication': 'DISABLED',
    },
    basic_auth=(
        JIRA_USERNAME,
        JIRA_PASSWORD
    )
)


def main():
    """Get issues from a given Jira Epic and save them as Gitlab issues."""
    issues = JIRA.search_issues(f'"Epic Link" = {JIRA_EPIC}', maxResults=0)

    gitlab_client = gitlab.Gitlab(GITLAB_URL, GITLAB_PRIVATE_TOKEN)
    gitlab_project = gitlab_client.projects.get(GITLAB_PROJECT)

    # Sort them reversed so the newest are created last
    for issue_id in reversed(issues):
        issue = JiraIssue(JIRA, issue_id)
        gl_issue = GitlabIssue.create_from_jira(gitlab_project, issue)

        issue.notify_migrated(gl_issue)

main()
